#ifndef READFILE
#define READFILE

char *readfile(FILE *fp);
int check_syntax(const char *filestr);

#endif
