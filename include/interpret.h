#ifndef INTERPRET
#define INTERPRET
#include "options.h"

void interpret(const char *commands, unsigned char *data,
        const struct options *optptr);

#endif
