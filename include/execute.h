#ifndef EXECUTE
#define EXECUTE
#include "options.h"

int execute_file(const char *filename, unsigned char *data,
        const struct options *optptr);
void execute_repl(unsigned char *data, const struct options *optpr);

#endif
