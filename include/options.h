#ifndef OPTIONS
#define OPTIONS

struct options {
    unsigned int debug : 1;
    size_t datasize;
};

int process_options(int argc, char **argv, struct options * const opt);

#endif
