#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "execute.h"

int main(int argc, char **argv)
{
    struct options opt = {0, 5000};  // debug 0, 5000 data array size default

    // save name and skip it
    char *progname = argv[0];
    ++argv;
    --argc;

    int remaining = process_options(argc, argv, &opt);
    if (opt.datasize < 1) {
        fprintf(stderr, "%s: error: datasize must be positive\n", progname);
        return 1;
    }
    argv += argc - remaining;  // move argv to first unprocessed argument
    argc = remaining;  // make argc represent the remaining argument count

    unsigned char *data = (unsigned char *) malloc(
            sizeof(unsigned char) * opt.datasize);
    if (data == NULL) {
        fprintf(stderr,"%s: error: couldn't allocate memory for array\n",
                progname);
        return 2;
    }
    memset(data, 0, sizeof(char) * opt.datasize);

    if (argc > 0) {  // file is to be read
        if (execute_file(*argv, data, &opt) != 0) {
            return 3;
        }
    } else {  // enter repl
        opt.debug = 1;  // always enable debug commands in repl
        execute_repl(data, &opt);
    }

    free(data);
    return 0;
}
