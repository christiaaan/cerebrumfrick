#include <stdio.h>
#include <stdlib.h>
#include "readfile.h"

// readfile: read a single character from file/stdin into the char array
char *readfile(FILE *fp)
{
    fseek(fp, 0, SEEK_END);
    long filesize = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    char *filestr = (char *) malloc(sizeof(char) * filesize);
    if (filestr == NULL) {  // error
        return filestr;
    }
    char *fileptr = filestr;

    int c;
    while ((c = getc(fp)) != '\0' && c != EOF) {
        // only add the character if it's a valid command
        switch (c) {
        case '>': case '<': case '+': case '-': case '.':
        case ',': case '[': case ']': case '!': case '#':
            *fileptr++ = c;
            break;
        default:
            break;
        }
    } 
    *fileptr = '\0';
    return filestr;
}

// check_syntax: checks whether opening and closing brackets match
int check_syntax(const char *filestr)
{
    const char *errormesg = "check_syntax: error:"
                    " unmatching opening and closing brackets";
    int loopdepth = 0;
    for (char *strptr = (char *) filestr; *strptr != '\0'; ++strptr) {
        if (*strptr == '[') {
            ++loopdepth;
        } else if (*strptr == ']') {
            --loopdepth;
        }
        if (loopdepth < 0) {  // closing brace before opening brace
            fprintf(stderr, "%s\n", errormesg);
            return loopdepth;
        }
    }

    if (loopdepth != 0) {
        fprintf(stderr, "%s\n", errormesg);
    }

    return loopdepth;
}
