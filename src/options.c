#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "options.h"

// numeric_option: set a numeric option to a given value
static int numeric_option(int * const option, char **argv)
{
    if (*++*argv != '=') {  // error
        return EOF;
    }
    *option = atoi(++*argv);
    // advance to next option (if there is one)
    while (isdigit(**argv)) {
        ++*argv;
    }
    --*argv;  // process_options skips one character by itself
    return 0;
}

// process_options: handles program options, returns remaining arguments
int process_options(int argc, char **argv, struct options * const optptr)
{
    int option;
    for ( ; argc > 0 && **argv == '-'; ++argv, --argc) {
        while (*++*argv != '\0') {
            switch (**argv) {
            case 'd':
                optptr->debug = 1;
                break;
            case 'm':
                if (numeric_option(&option, argv) == EOF) {
                    return EOF;
                } else {
                    optptr->datasize = option;
                }
                break;
            default:
                fprintf(stderr, "error: process_options:"
                        " invalid option %c\n", **argv);
                break;
            }
        }
    }
    return argc;
}
