#include <stdio.h>
#include <stdlib.h>
#include "interpret.h"
#include "options.h"
#include "readfile.h"
#include "execute.h"

int execute_file(const char *filename, unsigned char *data,
        const struct options *optptr)
{
    FILE *fp;
    if ((fp = fopen(filename, "r")) == NULL) {
        fprintf(stderr,"execute_file: error: can't open file %s\n", filename);
        return 1;
    }
    char *filestr = readfile(fp);
    if (filestr == NULL) {
        fprintf(stderr, "execute_file: error:"
                " couldn't allocate memory for file\n");
        return 2;
    }
    if (check_syntax(filestr)) {
        return 3;
    }
    fclose(fp);
    interpret(filestr, data, optptr);
    free(filestr);
    return 0;
}

// getcmd: print prompt and getline
int getcmd(const char *prompt, char **line, size_t *lim, FILE *fp)
{
    printf("%s", prompt);
    return getline(line, lim, fp);
}

void execute_repl(unsigned char *data, const struct options *optptr)
{
    char *line = NULL;
    size_t lim = 0;
    while (getcmd("\n* ", &line, &lim, stdin) > 0) {
        if (check_syntax(line)) {
            continue;
        }
        interpret(line, data, optptr);
    }
    free(line);
    putchar('\n');
}
