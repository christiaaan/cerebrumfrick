#include <stdio.h>
#include <stdlib.h>
#include "options.h"
#include "interpret.h"

static struct {
    unsigned int stack[500];
    unsigned int *ptr;
} loops = {{0}, loops.stack};

// looppush: push a coordinate to the stack of loops
static void looppush(const unsigned int i)
{
    if (loops.ptr - loops.stack < 500) {
        *loops.ptr++ = i;
    } else {
        // hinders the program from working correctly, so exit
        fprintf(stderr, "looppush: error: stack is full!\n");
        exit(4);
    }
}

// looppop: get the top coordinate from the loop stack
static int looppop(void)
{
    if (loops.ptr > loops.stack) {
        return *--loops.ptr;
    } else {
        // this is a serious error, should never happen
        fprintf(stderr, "looppop: error: stack is empty.\n");
        exit(4);
    }
}

// open_loop: handle start of loop (move dataptr past end or continue into loop)
static void open_loop(const unsigned char *dataptr, char **cmdptr,
        const char *commands)
{
    if (!*dataptr) {
        // skip loop
        int depth = 1;
        while (**cmdptr != '\0' && depth > 0) {
            if (**cmdptr == '[') {
                ++depth;
            } else if (**cmdptr == ']') {
                --depth;
            }
            ++*cmdptr;
        }
    } else {  // enter loop and push loop position to stack
        looppush(*cmdptr - commands - 1);
    }
}

// close_loop: handle end of loop (exit it or move dataptr back to the start)
static void close_loop(const unsigned char *dataptr, char **cmdptr,
        const char *commands)
{
    // pop loop off the stack, move pointer if *ptr is nonzero
    if (*dataptr) {
        *cmdptr = (char *) commands + looppop();
    } else {
        looppop();
    }
}

// mv_right: move data pointer to the right if possible
static void mv_right(unsigned char **dataptr, const unsigned char *data,
        const int datasize)
{
    if (*dataptr - data < datasize) {
        ++*dataptr;
    } else {
        fprintf(stderr, "interpret: warning: tried moving pointer "
                "off right side of array.\n");
    }
}

// mv_left: move data pointer to the left if possible
static void mv_left(unsigned char **dataptr, const unsigned char *data)
{
    if (*dataptr > data) {
        --*dataptr;
    } else {
        fprintf(stderr, "interpret: warning: tried moving pointer "
                "off left side of array.\n");
    }
}

// read_byte: read one byte of data and store it in dataptr
static void read_byte(unsigned char *dataptr)
{
    int c = getchar();
    if (c != EOF) {
        *dataptr = c;
    }
}

// print_data: prints the n+1 first cells of memory
static void print_data(const unsigned n, const unsigned char *data) {
    for (unsigned i = 0; i <= n; ++i) {
        if (i % 26 == 0) {
            putchar('\n');
        }
        printf(" %2.2x", data[i]);
    }
    putchar('\n');
}

// interpret: interpret commands until end of string is reached by cmdptr
void interpret(const char *commands, unsigned char *data,
        const struct options *optptr)
{
    static unsigned char *dataptr = NULL;
    static unsigned int rightmost_cell = 0;
    char *cmdptr = (char *) commands;

    // initialize dataptr and cmdptr
    if (dataptr == NULL) {
        dataptr = data;
    }

	// interpret all commands in file
    while (*cmdptr != '\0') {
        switch (*cmdptr++) {
        case '>':
            mv_right(&dataptr, data, optptr->datasize);
            break;
        case '<':
            mv_left(&dataptr, data);
            break;
        case '+':  // increment contents of dataptr
            ++*dataptr;
            break;
        case '-':  // decrement contents of dataptr
            --*dataptr;
            break;
        case '.':  // print contents of dataptr
            putchar(*dataptr);
            break;
        case ',':
            read_byte(dataptr);
            break;
        case '[':
            open_loop(dataptr, &cmdptr, commands);
            break;
        case ']':
            close_loop(dataptr, &cmdptr, commands);
            break;
        default:
            break;
        }

		// perform debug commands
        if (optptr->debug) {
            switch (*(cmdptr-1)) {
            case '>':
				// keep track of rightmost cell for # command
                if (rightmost_cell < dataptr - data) {
                    rightmost_cell = dataptr - data;
                }
                break;
            case '!':  // print current cell value as hex
                printf("%2.2x\n", *dataptr);
                break;
            case '#':  // print every cell from cell 0 to rightmost used cell
                print_data(rightmost_cell, data);
                break;
            default:
                break;
            }
        }
    }
}
