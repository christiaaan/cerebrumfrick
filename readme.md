Cerebrumfrick: A brainfuck interpreter.
=======================================

Usage:
    frick -options optional_filename 

Options:

* -d   : "debug" mode, adds the command ! which prints current register in hex, and #, which prints each cell from the leftmost one to the rightmost cell visited by the pointer
* -m=X : sets the size of the array, where X is the number of desired cells. (default 5000)

If a filename is given, it is read into memory and executed as a script by the interpreter. If no filename is given, however, the program launches into an interactive mode, where it executes each line given by the user as its own program (keeping state the same across each line). In interactive mode, debug commands are always enabled.
